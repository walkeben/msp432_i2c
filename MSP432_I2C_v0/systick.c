#include "msp.h"
#include <stdio.h>
#include <stdlib.h>
#include "systick.h"

void systick_init_ms(void) {
/***| systick_init_ms() |************************************
*Brief:     Initializes and loads the System Tick Clock for a 1 ms delay.
*Params:    N/A
*Returns:   N/A
********************************************************/
    SysTick->LOAD = 3000 - 1;       // 3000 cycles = 1 ms
    SysTick->VAL = 0;               // Set current SysTick timer value to zero
    SysTick->CTRL = 0x05;           // SysTick clock enabled without interrupt
}

void systick_delay_ms(uint16_t delayms) {
/***| systick_delay_ms() |************************************
*Brief:     Uses the System Tick Clock to make a 1 ms delay.
*Params:    N/A
*Returns:   N/A
*Dependencies:      systick_init_ms
********************************************************/
    uint16_t i = 0;
    systick_init_ms();
    for (i = 0; i < delayms; i++) {
        while ( (SysTick->CTRL & 0x10000) == 0 );    // Stay here until SysTick reaches zero
    }
}
