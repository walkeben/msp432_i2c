#include "msp.h"
#include "systick.h"


/**
 * main.c
 */
void main(void)
{
	WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;		// stop watchdog timer
}

void init_timera0() {

}

// #region Timer A0 Functions
void init_ta0() {
/**| init_ta0() |**********************************************
 * Brief:   Initializes Timer A0 on P2.7 to act as a PWM signal
 *          to drive a DC motor.
 *
 * Params: global
 * Returns: N/A
 * *******************************************************/
    // Instance Timer A0.4 is on pin 2.7
    P2->SEL0 |= BIT7;
    P2->SEL1 &= ~BIT7;
    P2->DIR |= BIT7;

    // TimerA0.4 initialized as a PWM
    if (duty_cycle > 100)  duty_cycle = 100.0;  // Make sure input is within bounds
    TIMER_A0->CCR[0] = OP_FREQ * 1500;          // Calculate and set period length
    TIMER_A0->CCR[4] = duty_cycle / 100.0 * (OP_FREQ * 1500.0);     // Set duty cycle
    TIMER_A0->CCTL[4] = 0xE0;       // CCR4 Reset/Set Mode
    TIMER_A0->CTL = 0x214;          // Use SMCLK, count up to CCR0 to overflow, clear TA0R register
    TIMER_A0->EX0 = 1;              // /2 divider
} // #endregion
